﻿using Abstraction.Module.User;
using DAL.Repository.IRepository;
using DAL.Repository.RespositoryFactory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entity.ModelEntity.User;

namespace DAL.RepositoryFacade
{
    public class UserRepositoryFacade : UserAbstract
    {
        #region Declaration
        private Lazy<IAddUserRepository> _adduserRepositoryObj = null;

        private Lazy<IGetTimeStampRepository> _getTimeStampRepositoryObj = null;
       
        #endregion

        #region Constructor
        public UserRepositoryFacade()
        {

        }
        #endregion

        #region Private Property (Sub Repository Instance)
        private Lazy<IAddUserRepository> AdduserRepositoryInstance
        {
            get
            {
                return
                       _adduserRepositoryObj
                            ??
                                (_adduserRepositoryObj = new Lazy<IAddUserRepository>(() => UserRepositoryFactory.ExecuteFactory<IAddUserRepository>(UserRepositoryFactory.RepositoryType.AddUserRepository)));

            }
        }

        private Lazy<IGetTimeStampRepository> GetTimeStampRepositoryInstance
        {
            get
            {
                return
                       _getTimeStampRepositoryObj
                            ??
                                (_getTimeStampRepositoryObj = new Lazy<IGetTimeStampRepository>(() => UserRepositoryFactory.ExecuteFactory<IGetTimeStampRepository>(UserRepositoryFactory.RepositoryType.GetTimeStampRepository)));

            }
        }

        #endregion

        #region Public Method

        public override void Dispose()
        {
            _adduserRepositoryObj?.Value?.Dispose();
            _adduserRepositoryObj = null;

            _getTimeStampRepositoryObj?.Value?.Dispose();
            _getTimeStampRepositoryObj = null;
        }

        public async override Task<dynamic> GetTimeStampAsync(UserEntity userEntityObj)
        {
            try
            {
                return await this.GetTimeStampRepositoryInstance
                    ?.Value
                    ?.GetTimeStampData(userEntityObj);
            }
            catch(Exception)
            {
                throw;
            }
        }


        public async override Task<dynamic> AddUserAsync(List<UserEntity> userEntityObj)
        {
            try
            {
                return await this.AdduserRepositoryInstance
                    ?.Value
                    ?.AddUserData(userEntityObj);
            }
            catch(Exception)
            {
                throw;
            }
        }

        #endregion
    }
}
