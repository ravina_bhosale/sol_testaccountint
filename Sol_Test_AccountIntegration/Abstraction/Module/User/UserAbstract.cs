﻿using Entity.IModelEntity.User;
using Entity.ModelEntity.User;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Abstraction.Module.User
{
    public abstract class UserAbstract : IDisposable
    {
        public abstract void Dispose();

        // Get Max TimeStamp
        public abstract Task<dynamic> GetTimeStampAsync(UserEntity userEntityObj);

        // insert collction of data
        public abstract Task<dynamic> AddUserAsync(List<UserEntity> userEntityObj);
    }
}
