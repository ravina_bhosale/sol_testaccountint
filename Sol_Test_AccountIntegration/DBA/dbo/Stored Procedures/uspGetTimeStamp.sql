﻿CREATE PROCEDURE uspGetTimeStamp
(
	@Command Varchar(MAX)=NULL,

	@SubscriptionUniqueId Varchar(50)=NULL,
	@SocietyNo numeric(18,0)=NULL,

	@Status int=NULL OUT,
	@Message Varchar(MAX)= NULL OUT
)

AS
	BEGIN
		DECLARE @ErrorMessage Varchar(MAX)=NULL

		IF @Command='GetTimeStamp'

			BEGIN

				BEGIN TRANSACTION

				BEGIN TRY

					SELECT 
					'UserModule' AS ModuleName,
					Max(U.SyncTimeStamp) AS MAXTimeStamp
					FROM tblOfflineUser U
					WHERE SubscriptionUniqueId=@SubscriptionUniqueId AND
						  SocietyNo=@SocietyNo

					SET @Status=1
					SET @Message='Getting OfflineModule'

				COMMIT TRANSACTION

			END TRY

			BEGIN CATCH

				SET @ErrorMessage=ERROR_MESSAGE()
				SET @Status=0
				SET @Message='Stored Proc execution Fail'

				RAISERROR(@ErrorMessage,16,1)

			END CATCH
				
			END
	END
	GO
