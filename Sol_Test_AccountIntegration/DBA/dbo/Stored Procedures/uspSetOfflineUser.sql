﻿CREATE PROCEDURE uspSetOfflineUser
(
	@Command Varchar(MAX)=NULL,

	@OfflineId Numeric(18,0)=NULL,
	@SubscriptionUniqueId Varchar(50)=NULL,
	@FirstName Varchar(50)=NULL,
	@LastName Varchar(50)=NULL,
	@Age Numeric(18,0)=NULL,
	@SocietyNo Numeric(18,0)=NULL,

	@Status int=NULL OUT,
	@Message Varchar(MAX)= NULL OUT
)

AS
	BEGIN

		DECLARE @ErrorMessage Varchar(MAX)=NULL
		DECLARE @TempOfflineId numeric(18,0)=null

		IF @Command='CollectData'

		BEGIN

			BEGIN TRANSACTION

			BEGIN TRY

				SELECT 
				  @TempOfflineId=U.OfflineUserId
					FROM tblOfflineUser AS U
						WHERE U.SubscriptionUniqueId=@SubscriptionUniqueId AND
							  U.SocietyNo=@SocietyNo AND
							  U.OfflineUserId=@OfflineId

					IF @TempOfflineId IS NULL
						BEGIN
							INSERT INTO tblOfflineUser
							(
								OfflineUserId,
								SubscriptionUniqueId,
								FirstName,
								LastName,
								Age,
								SocietyNo,
								SyncTimeStamp
							)
							VALUES
							(
								@OfflineId,
								@SubscriptionUniqueId,
								@FirstName,
								@LastName,
								@Age,
								@SocietyNo,
								GETDATE()
							)

							SET @Status=1
							SET @Message='Insert Successfully'

						END
					ELSE
						BEGIN
							UPDATE tblOfflineUser
							SET FirstName=@FirstName,
								LastName=@LastName,
								Age=@Age
							WHERE OfflineUserId=@OfflineId AND 
								  SubscriptionUniqueId=@SubscriptionUniqueId AND
								  SocietyNo=@SocietyNo

							SET @Status=1
							SET @Message='Update Successfully'
						END

				
				COMMIT TRANSACTION

			END TRY

			BEGIN CATCH

				SET @ErrorMessage=ERROR_MESSAGE()
				SET @Status=0
				SET @Message='Stored Proc execution Fail'

				RAISERROR(@ErrorMessage,16,1)
					
				ROLLBACK TRANSACTION

			END CATCH

		END

	END
	GO
