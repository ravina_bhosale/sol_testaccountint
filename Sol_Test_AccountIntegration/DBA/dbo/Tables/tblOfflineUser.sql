﻿CREATE TABLE [dbo].[tblOfflineUser] (
    [UserId]               NUMERIC (18) IDENTITY (1, 1) NOT NULL,
    [OfflineUserId]        NUMERIC (18) NULL,
    [SubscriptionUniqueId] VARCHAR (50) NULL,
    [FirstName]            VARCHAR (50) NULL,
    [LastName]             VARCHAR (50) NULL,
    [Age]                  NUMERIC (18) NULL,
    [SocietyNo]            NUMERIC (18) NULL,
    [SyncTimeStamp]        DATETIME     NULL,
    CONSTRAINT [PK_tblOfflineUser] PRIMARY KEY CLUSTERED ([UserId] ASC)
);

