﻿using DAL.ORM.Interface.IConcrete.User;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAL.ORM.ORD;
using Entity.ModelEntity.User;

namespace DAL.ORM.Concrete.User
{
    public class UserConcrete : IUserConcrete
    {

        #region Declaration

        private Lazy<UserDcDataContext> _db = null;

        private static Lazy<UserConcrete> _userConcreteObj = null;

        #endregion

        #region Constructor

        private UserConcrete()
        {

        }

        #endregion

        #region Property

        private Lazy<UserDcDataContext> DbInstance
        {
            get
            {
                return
                    _db
                    ??
                    (_db = new Lazy<UserDcDataContext>(() => new UserDcDataContext()));
            }
        }

        public static Lazy<UserConcrete> CreateInstance
        {
            get
            {
                return
                    _userConcreteObj
                        ??
                        (_userConcreteObj = new Lazy<UserConcrete>(() => new UserConcrete()));
            }
        }

        #endregion

        #region Public Method

        public void Dispose()
        {
            _db?.Value?.Dispose();
            _db = null;

            _userConcreteObj = null;
        }


        public async Task<dynamic> SetDataAsync(string command, UserEntity entity, Action<int?, string> storedProcOutPara = null)
        {
            int? status = null;
            string message = null;
            try
            {
                return await Task.Run(() =>
                {
                    var setQuery =
                    DbInstance
                    ?.Value
                    ?.uspSetOfflineUser
                    (
                        command,
                        entity?.OfflineUserId,
                        entity?.Subscription?.SubscriptionUniqueId,
                        entity?.FirstName,
                        entity?.LastName,
                        entity?.Age,
                        entity?.SocietyNo,
                        ref status,
                        ref message
                        );

                    // get out parameter value from SP.
                    storedProcOutPara(status, message);

                    return setQuery;

                });
            }
            catch (Exception)
            {
                throw;
            }

        }

        public async Task<dynamic> GetDataAsync(string command, UserEntity entityObj, Func<GetTimeStampResultSet, UserEntity> selectQuery, Action<int?, string> storedProcOutPara = null)
        {
            int? status = null;
            string message = null;
            try
            {
                return await Task.Run(() =>
                {
                    var getQuery =
                    DbInstance
                    ?.Value
                    ?.uspGetTimeStamp
                    (
                        command,
                        entityObj?.Subscription?.SubscriptionUniqueId,
                        entityObj?.SocietyNo,
                        ref status,
                        ref message
                     )
                    ?.AsEnumerable()
                    ?.Select(selectQuery)
                    ?.ToList();

                    return getQuery;
                });
            }
            catch (Exception)
            {
                throw;
            }


            #endregion
        }
    }
}
