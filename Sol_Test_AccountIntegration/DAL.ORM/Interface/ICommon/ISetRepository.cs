﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.ORM.Interface.ICommon
{
    public interface ISetRepository<TEntity> where TEntity : class
    {
        // This is common repository for insert,update,delete data using in all modules

        Task<dynamic> SetDataAsync(string command, TEntity entity, Action<int?,string> storedProcOutPara = null); 
    }
}
