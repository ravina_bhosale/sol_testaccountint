﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.ORM.Interface.ICommon
{
    public interface IGetRepository<TEntity, TLinqEntity> where TEntity : class where TLinqEntity : class
    {
       // This is common repository for get data using in all modules

        Task<dynamic> GetDataAsync
            (string command,
            TEntity entityObj, 
            Func<TLinqEntity, TEntity> selectQuery, 
            Action<int?, string> storedProcOutPara = null);
    }
}
