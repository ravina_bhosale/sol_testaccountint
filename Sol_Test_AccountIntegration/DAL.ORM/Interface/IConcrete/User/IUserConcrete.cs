﻿using DAL.ORM.Interface.ICommon;
using DAL.ORM.ORD;
using Entity.ModelEntity.User;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.ORM.Interface.IConcrete.User
{
    public interface IUserConcrete : ISetRepository<UserEntity>,IGetRepository<UserEntity, GetTimeStampResultSet>,IDisposable
    {
    }
}
