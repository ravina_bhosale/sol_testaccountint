﻿using Entity.ModelEntity.User;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity.IModelEntity
{
    public interface IModuleEntity
    {
        String ModuleName { get; set; }

        DateTime? MAXTimeStamp { get; set; }
    }
}
