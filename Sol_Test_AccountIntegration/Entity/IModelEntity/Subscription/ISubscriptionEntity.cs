﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity.IModelEntity.Subscription
{
    public interface ISubscriptionEntity
    {
        String SubscriptionUniqueId { get; set; }

        String SubscriptionName { get; set; }
    }
}
