﻿using Entity.ModelEntity;
using Entity.ModelEntity.Subscription;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity.IModelEntity.User
{
    public interface IUserEntity
    {
        decimal? UserId { get; set; }

        decimal? OfflineUserId { get; set; }
        
        String FirstName { get; set; }
        
        String LastName { get; set; }

        int? Age { get; set; }

        int? SocietyNo { get; set; }
        
        DateTime SyncTimeStamp { get; set; }

        SubscriptionEntity Subscription { get; set; }

        ModuleEntity Module { get; set; }


    }
}
