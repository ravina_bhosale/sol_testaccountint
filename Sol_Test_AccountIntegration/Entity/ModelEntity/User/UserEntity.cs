﻿using Entity.IModelEntity.User;
using Entity.ModelEntity.Subscription;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity.ModelEntity.User
{
    public class UserEntity :IUserEntity
    {
        public decimal? UserId { get; set; }

        public decimal? OfflineUserId { get; set; }

        public String FirstName { get; set; }

        public String LastName { get; set; }

        public int? Age { get; set; }

        public int? SocietyNo { get; set; }

        public DateTime SyncTimeStamp { get; set; }

        public SubscriptionEntity Subscription { get; set; }

        public ModuleEntity Module { get; set; }
    }
}
