﻿using Entity.IModelEntity.Subscription;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity.ModelEntity.Subscription
{
    public class SubscriptionEntity :ISubscriptionEntity
    {
        public String SubscriptionUniqueId { get; set; }

        public String SubscriptionName { get; set; }
    }
}
