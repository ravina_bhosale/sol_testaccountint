﻿using Entity.IModelEntity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity.ModelEntity
{
    public class ModuleEntity : IModuleEntity
    {
       public String ModuleName { get; set; }

        public DateTime? MAXTimeStamp { get; set; }
    }
}
