﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Threading.Tasks;
using Entity.ModelEntity.User;
using System.Collections.Generic;
using BAL.Context;
using Entity.ModelEntity.Subscription;

namespace UTP.ModuleTest
{
    [TestClass]
    public class UserUnitTest
    {
        #region Bal to database

        //[TestMethod]
        //public void UserTestMethod()
        //{
        //    Task.Run(async() =>
        //    {
        //        List<UserEntity> userlistObj = new List<UserEntity>()
        //        {
        //            new UserEntity()
        //            {
        //                OfflineUserId=2,
        //                FirstName="Sameer",
        //                LastName="Dalvi",
        //                Age=30,
        //                SocietyNo=1,
        //               Subscription=new SubscriptionEntity()
        //               {
        //                   SubscriptionUniqueId="R#1"
        //               }
        //            },
        //            new UserEntity()
        //            {
        //                OfflineUserId=3,
        //                FirstName="Sakshi",
        //                LastName="Pawar",
        //                Age=21,
        //                SocietyNo=2,
        //                Subscription=new SubscriptionEntity()
        //               {
        //                   SubscriptionUniqueId="R#1"
        //               }
        //            },
        //            new UserEntity()
        //            {
        //                OfflineUserId=1,
        //                FirstName="Radha",
        //                LastName="Shinde",
        //                Age=27,
        //                SocietyNo=1,
        //                Subscription=new SubscriptionEntity()
        //                {
        //                    SubscriptionUniqueId="P#1"
        //                }
        //            }
        //        };
        //        var Result = await new UserContext()?.AddUserAsync(userlistObj);
        //        Assert.IsNotNull(Result);
        //    }).Wait();
        //}

        //[TestMethod]
        //public void GetTimeStampTestMethod()
        //{
        //    Task.Run(async() =>
        //    {
        //        var getTimeStampObj = new UserEntity()
        //        {
        //            SocietyNo = 2,
        //            Subscription = new SubscriptionEntity()
        //            {
        //                SubscriptionUniqueId = "R#1",
        //            }
        //        };
        //        var result = await new UserContext()?.GetTimeStampAsync(getTimeStampObj);
        //        Assert.IsNotNull(result);
        //    }).Wait();
        //}

        #endregion


        #region Api To Database
        //[TestMethod]
        //public void AddUserApiTestMethod()
        //{
        //    Task.Run(async () =>
        //    {
        //        List<UserEntity> data = new List<UserEntity>()
        //        {
        //         new UserEntity()
        //         {
        //             OfflineUserId = 2,
        //             FirstName = "Naziya",
        //             LastName = "Khan",
        //             Age = 24,
        //             SocietyNo = 1,
        //             Subscription = new SubscriptionEntity()
        //             {
        //                 SubscriptionUniqueId = "P#1"
        //             }
        //         },
        //            new UserEntity()
        //            {
        //                OfflineUserId = 3,
        //                FirstName = "Sakshi",
        //                LastName = "Pawar",
        //                Age = 25,
        //                SocietyNo = 2,
        //                Subscription = new SubscriptionEntity()
        //                {
        //                    SubscriptionUniqueId = "R#1"
        //                }
        //            },
        //            };

        //        string apiUrl = "http://localhost:60169/Services/User/UserService.svc/AddUserAsync";

        //        var result = await WCFRest.PostMessage<List<UserEntity>>(data, apiUrl);

        //        Assert.IsNotNull(result);


        //    }).Wait();
        //}


        [TestMethod]
        public void GetTimeStampAPiTestMethod()
        {
            Task.Run(async () =>
            {
                var data = new UserEntity()
                {
                    SocietyNo = 2,
                    Subscription = new SubscriptionEntity()
                    {
                        SubscriptionUniqueId = "R#1",
                    }
                };
                string apiUrl = "http://localhost:60169/Services/User/GetTimeStampService.svc/GetMaxTimeStamp";

                var result = await WCFRest.PostMessage<UserEntity>(data, apiUrl);

                Assert.IsNotNull(result);
            }).Wait();
        }

    }
    #endregion
}

