﻿using DAL.RepositoryFacade;
using Entity.IModelEntity.User;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entity.ModelEntity.User;

namespace BAL.Context
{
    public class UserContext : UserRepositoryFacade
    {
        #region Constructor
        public UserContext() : base()
        {

        }
        #endregion

        #region Public Override Method
        public override Task<dynamic> AddUserAsync(List<UserEntity> userEntityObj)
        {
            try
            {
                return base.AddUserAsync(userEntityObj);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public override Task<dynamic> GetTimeStampAsync(UserEntity userEntityObj)
        {
            try
            {
                return base.GetTimeStampAsync(userEntityObj);
            }
            catch(Exception)
            {
                throw;
            }
        }

        #endregion
    }
}
