﻿using Entity.ModelEntity.User;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Repository.IRepository
{
    public interface IAddUserRepository : IDisposable
    {
        Task<dynamic> AddUserData(List<UserEntity> userEntityObj);
    }
}
