﻿using DAL.Repository.IRepository;
using DAL.Repository.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Repository.RespositoryFactory
{
    public class UserRepositoryFactory
    {
        #region enum 
        public enum RepositoryType
        {
            AddUserRepository = 0,
            GetTimeStampRepository=1

        };
        #endregion

        #region Declaration
        private static Dictionary<RepositoryType, dynamic> _dicObj = new Dictionary<RepositoryType, dynamic>();
        #endregion

        #region Constructor
        static UserRepositoryFactory()
        {
            AddRepositoryInstance();
        }
        #endregion

        #region Private Method
        private static void AddRepositoryInstance()
        {
            _dicObj.Add(RepositoryType.AddUserRepository, new Lazy<IAddUserRepository>(() => AddUserRepository.CreateInstance.Value));

            _dicObj.Add(RepositoryType.GetTimeStampRepository, new Lazy<IGetTimeStampRepository>(() => GetTimeStampRepository.CreateInstance.Value));

        }
        #endregion

        #region Public Method
        public static TRepository ExecuteFactory<TRepository>(RepositoryType repositoryTypeObj) where TRepository : class
        {
            return _dicObj[repositoryTypeObj].Value as TRepository;
        }
        #endregion 
    }
}
