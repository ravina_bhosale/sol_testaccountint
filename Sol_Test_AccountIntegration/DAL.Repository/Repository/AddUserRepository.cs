﻿using DAL.Repository.IRepository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entity.IModelEntity.User;
using DAL.ORM.Interface.IConcrete.User;
using DAL.ORM.ConcreteFactory;
using Entity.ModelEntity.User;

namespace DAL.Repository.Repository
{
    public class AddUserRepository : IAddUserRepository
    {
        #region declaration
        private Lazy<IUserConcrete> _userConcreteObj = null;

        private static Lazy<AddUserRepository> _addUserRepositoryObj = null;

        #endregion


        #region Constructor
        private AddUserRepository()
        {

        }
        #endregion

        #region Property
        private Lazy<IUserConcrete> UserConcreteInstance
        {
            get
            {
                return
                        _userConcreteObj
                            ??
                                (_userConcreteObj = new Lazy<IUserConcrete>(() => FactoryConcrete.ExecuteFactory<IUserConcrete>(FactoryConcrete.ConcreteType.User)));
            }
        }

        public static Lazy<AddUserRepository> CreateInstance
        {
            get
            {
                return
                       _addUserRepositoryObj
                            ??
                                (_addUserRepositoryObj = new Lazy<AddUserRepository>(() => new AddUserRepository()));
            }
        }

        #endregion

        #region public method

        public void Dispose()
        {
            _userConcreteObj?.Value?.Dispose();
            _userConcreteObj = null;

            _addUserRepositoryObj = null;
        }

        public async Task<dynamic> AddUserData(List<UserEntity> userEntityObj)
        {
            int? status = null;
            string message = null;
            try
            {
                foreach(UserEntity entityObj in userEntityObj)
                {
                    await UserConcreteInstance
                           ?.Value
                           ?.SetDataAsync
                           (
                               "CollectData",
                               entityObj,
                               (lestatus, lemessage) =>
                               {
                                   status = lestatus;
                                   message = lemessage;
                               });
                }
                return message;
            }
            catch(Exception)
            {
                throw;
            }
        }

        #endregion
    }
}
