﻿using DAL.Repository.IRepository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entity.ModelEntity.User;
using DAL.ORM.Interface.IConcrete.User;
using DAL.ORM.ConcreteFactory;
using Entity.ModelEntity.Subscription;
using Entity.ModelEntity;

namespace DAL.Repository.Repository
{
    public class GetTimeStampRepository : IGetTimeStampRepository
    {
        #region declaration
        private Lazy<IUserConcrete> _userConcreteObj = null;

        private static Lazy<GetTimeStampRepository> _getTimeStampRepositoryObj = null;

        #endregion


        #region Constructor
        private GetTimeStampRepository()
        {

        }
        #endregion

        #region Property
        private Lazy<IUserConcrete> UserConcreteInstance
        {
            get
            {
                return
                        _userConcreteObj
                            ??
                                (_userConcreteObj = new Lazy<IUserConcrete>(() => FactoryConcrete.ExecuteFactory<IUserConcrete>(FactoryConcrete.ConcreteType.User)));
            }
        }

        public static Lazy<GetTimeStampRepository> CreateInstance
        {
            get
            {
                return
                       _getTimeStampRepositoryObj
                            ??
                                (_getTimeStampRepositoryObj = new Lazy<GetTimeStampRepository>(() => new GetTimeStampRepository()));
            }
        }

        #endregion

        #region Public Method

        public void Dispose()
        {
            _userConcreteObj?.Value?.Dispose();
            _userConcreteObj = null;

            _getTimeStampRepositoryObj = null;
        }

        public async Task<dynamic> GetTimeStampData(UserEntity userEntityObj)
        {
            try
            {
                var getquery= await
                    UserConcreteInstance
                    ?.Value
                    ?.GetDataAsync
                    (
                        "GetTimeStamp",
                        userEntityObj,
                        (leLinqGetTimeStampObj) => new UserEntity()
                        {
                            Module = new ModuleEntity()
                            {

                                ModuleName = leLinqGetTimeStampObj.ModuleName,
                                MAXTimeStamp = leLinqGetTimeStampObj.MAXTimeStamp
                            }
                        }
                        );

                return getquery;
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion
    }
}
