﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using Entity.ModelEntity.User;
using BAL.Context;
using System.ServiceModel.Activation;

namespace APi.Services.User
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "GetTimeStampService" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select GetTimeStampService.svc or GetTimeStampService.svc.cs at the Solution Explorer and start debugging.
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]

    public class GetTimeStampService : IGetTimeStampService
    {
        public async Task<object> GetMaxTimeStamp(UserEntity userEntityObj)
        {
            try
            {
                UserContext userContextObj = new UserContext();
                return await userContextObj.GetTimeStampAsync(userEntityObj);
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
