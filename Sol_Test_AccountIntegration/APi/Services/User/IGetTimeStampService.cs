﻿using Entity.ModelEntity.User;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using System.Threading.Tasks;

namespace APi.Services.User
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IGetTimeStampService" in both code and config file together.
    [ServiceContract]
    public interface IGetTimeStampService
    {
        [OperationContract]
        [WebInvoke(Method = "POST",
         RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json,
         UriTemplate = "GetMaxTimeStamp")]
        [ServiceKnownType(typeof(UserEntity))]
        Task<object> GetMaxTimeStamp(UserEntity userEntityObj);

    }
}
